(function(w){
  var dl;
  var Hook;
  var HookButton;
  var HookInput;
  var DropDown;

  (function(w){
    // Custom event support for IE
    if ( typeof window.CustomEvent === "function" ) return false;
    w.CustomEvent = function ( event, params ) {
      params = params || { bubbles: false, cancelable: false, detail: undefined };
      var evt = document.createEvent( 'CustomEvent' );
      evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
      return evt;
    }
    CustomEvent.prototype = window.Event.prototype;
  }(w));

  w.DropLab = function(hook){
    if(typeof hook !== 'undefined'){
        this.addHook(hook);
    }
    this.addEvents();
  };


  dl = w.DropLab;
  
  dl.DATA_TRIGGER = 'data-dropdown-trigger';
  dl.DATA_DROPDOWN = 'data-dropdown';
  
  dl.utils = {
    toDataCamelCase: function(attr){
      return this.camelize(attr.split('-').slice(1).join(' '));
    },

    // the tiniest damn templating I can do
    t: function(s,d){
      for(var p in d)
        s=s.replace(new RegExp('{{'+p+'}}','g'), d[p]);
      return s;
    },

    camelize: function(str) {
      return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
        return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
      }).replace(/\s+/g, '');
    },

    closest: function(thisTag, stopTag) {
      while(thisTag.tagName !== stopTag && thisTag.tagName !== 'HTML'){
        thisTag = thisTag.parentNode;
      }
      return thisTag;
    }, 
    isDropDownParts: function(target) {
      if(target.tagName === 'HTML') { return false };
      return (target.dataset.hasOwnProperty(
      this.toDataCamelCase(
        dl.DATA_TRIGGER)
      ) || 
      target.dataset.hasOwnProperty(
        this.toDataCamelCase(dl.DATA_DROPDOWN)
        ) 
      )
    }
  }
  
  dl.prototype.hooks = [];

  dl.prototype.addData = function(trigger, data) {

  };

  dl.prototype.setData = function(trigger, data) {
    this.hooks.forEach(function(hook) {
      if(hook.trigger.dataset.hasOwnProperty('id')) {
        if(hook.trigger.dataset.id === trigger) {
          hook.list.setData(data);
        }
      }
    })
  }

  dl.prototype.addEvents = function(e) {
    var self = this;
    w.addEventListener('click', function(e){
      var thisTag = e.target;
      if(thisTag.tagName === 'LI' || thisTag.tagName === 'A'){
        // climb up the tree to find the UL
        thisTag = dl.utils.closest(thisTag, 'UL');
      }
      if(dl.utils.isDropDownParts(thisTag)){ return }
      if(dl.utils.isDropDownParts(e.target)){ return }
      self.hooks.forEach(function(hook) {
        hook.list.hide();
      })
    })
  }
  
  dl.prototype.addHook = function(hook){
    var list;
    if(hook instanceof HTMLElement){
      
    } else if(typeof hook === 'String'){
      hook = document.querySelector(hook);
    }
    list = document.querySelector(hook.dataset[dl.utils.toDataCamelCase(dl.DATA_TRIGGER)]);
    if(hook.tagName === 'A' || hook.tagName === 'button') {
      this.hooks.push(new dl.HookButton(hook, list));
    } else if(hook.tagName === 'INPUT') {
      this.hooks.push(new dl.HookInput(hook, list));
    }
  };
  dl.DropDown = function(list, trigger) {
    this.hidden = true;
    this.list = list;
    this.trigger = trigger;
    this.items = [];
    this.getItems();
    this.addEvents();
  };

  DropDown = dl.DropDown;
  DropDown.prototype.getItems = function() {
    this.items = this.list.querySelectorAll('li');
  };

  DropDown.prototype.addEvents = function() {
    var self = this;
    // event delegation.
    this.list.addEventListener('click', function(e) {
      if(e.target.tagName === 'A') {
        self.hide();
        var listEvent = new CustomEvent('click.dl', {
          detail: {
            list: self,
            selected: e.target,
            data: e.target.dataset
          }
        });
        self.list.dispatchEvent(listEvent);
      }
    });
  }

  DropDown.prototype.toggle = function() {
    if(this.hidden) {
      this.show();
    } else {
      this.hide();
    }
  };

  DropDown.prototype.setData = function(data) {
    // empty the list first
    var sampleItem;
    var anchor = false;
    var newChildren = [];
    var toAppend;
    var parent;
    this.items.forEach(function(item) {
      sampleItem = item;
      parent = item.parentNode;
      if(item.parentNode.dataset.hasOwnProperty('dynamic')) {
        item.parentNode.removeChild(item);  
      }
      
    });

    newChildren = data.map(function(dat){
      return dl.utils.t(sampleItem.outerHTML, dat);
    });
    toAppend = this.list.querySelector('ul[data-dynamic]');
    if(!!toAppend) {
      toAppend.innerHTML = newChildren.join('');
    } else {
      this.list.innerHTML = newChildren.join('');  
    }
  }

  DropDown.prototype.show = function() {
    this.list.style.display = 'block';
    this.hidden = false;
  };

  DropDown.prototype.hide = function() {
    this.list.style.display = 'none';
    this.hidden = true;
  };
  
  dl.Hook = function(trigger, list){
    this.trigger = trigger;
    this.list = new dl.DropDown(list);
    this.type = 'Hook';
    this.event = 'click';
  };

  Hook = dl.Hook;
  Hook.prototype.addEvents = function(){};
  Hook.prototype.constructor = dl.Hook;
  
  dl.HookButton = function(trigger, list) {
    dl.Hook.call(this, trigger, list);
    this.type = 'button';
    this.event = 'click';
    this.addEvents();
  };

  HookButton = dl.HookButton;

  HookButton.prototype = Object.create(dl.Hook.prototype);
  HookButton.prototype.addEvents = function(){
    var self = this;
    this.trigger.addEventListener('click', function(e){
      var buttonEvent = new CustomEvent('click.dl', {
        detail: {
          hook: self
        }
      });
      self.list.show();
      e.target.dispatchEvent(buttonEvent);
    });
  }

  HookButton.prototype.constructor = dl.HookButton;

  dl.HookInput = function(trigger, list) {
    dl.Hook.call(this, trigger, list);
    this.type = 'input';
    this.event = 'input';
    this.addEvents();
  };

  HookInput = dl.HookInput;
  HookInput.prototype.addEvents = function(){
    var self = this;
    this.trigger.addEventListener('input', function(e){
      var inputEvent = new CustomEvent('input.dl', {
        detail: {
          hook: self,
          text: e.target.value
        }
      });
      e.target.dispatchEvent(inputEvent);
      self.list.show();
    });
  };
  
  dl.prototype._AutoInitialize = function(){
    var dropdownTriggers = document.querySelectorAll('['+dl.DATA_TRIGGER+']');
    if(dropdownTriggers.length){
      var dropdown = new w.DropLab();
      w.droplab = dropdown
      dropdownTriggers.forEach(function(trigger){
        dropdown.addHook(trigger);
      });
      var readyEvent = new CustomEvent('ready.dl', {
        detail: {
          dropdown: dropdown
        }
      });
      w.dispatchEvent(readyEvent);
      dropdown.ready = true;
    }
  };
  dl.prototype.ready = false;
  dl.prototype._AutoInitialize();

  window.CustomEvent = CustomEvent;

}(window));